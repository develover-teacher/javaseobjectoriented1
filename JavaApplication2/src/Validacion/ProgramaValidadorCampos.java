package Validacion;

import animales.*;

/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: 
 * -validaCampNoBuit(String camp) 
 * -validaTamanyMaximCamp(String camp, int tamanyMaxCamp) 
 * -validaCampEmail(String camp) 
 * - validaIntervalNumero(int intMinim, int intMaxim)
 */
public class ProgramaValidadorCampos {

    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_NO_BUIT = "El camp està informat?";
    static final String CAMP_PRIM_MAJUSCULES = "Primera lletra majuscula ?";
    static final String CAMP_INTERNVAL = "El camp sobrepassa l'interval indicat?";
    static final String CAMP_EMAIL = "El camp és un email vàlid?";
    static final String CAMP_URL = "El camp és una adreça web vàlida?";
    
    // Converteix un boolea en una resposta de Si o No que mostra 
    // per pantalla.
    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if(respostaAfirmativa)
            System.out.println(SI);
        else
            System.out.println(NO);
    }
    
    public static void main(String[] args) {

        ValidadorCampos validador = new ValidadorCampos();
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("Bones")); 
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("")); 
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit(null)); 
        
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("Bones")); 
         System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("bones")); 
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("boNsES")); 
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("")); 
        
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("Nombre",20));
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789",5));
        
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("Nombre"));
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("miquel.angel.amoros@gmail.com"));
        
        System.out.println(CAMP_URL + " Nombre");
        imprimeixResposta(validador.validaCampURL("Nombre"));
        String website1 = "https://www.apache.org/";
        System.out.println(CAMP_URL + " " + website1);
        imprimeixResposta(validador.validaCampURL(website1));
        String website2 = "eldiario.es";
        System.out.println(CAMP_URL + " " + website2);
        imprimeixResposta(validador.validaCampURL(website2));
        
   }
  
}
