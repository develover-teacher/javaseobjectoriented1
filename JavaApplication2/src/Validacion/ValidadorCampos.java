package Validacion;
import java.util.*;

/** 
23. 
* Crea una classe Validador que servirà per verificar camps de formularis; 
* i una classe Main que provi tots els mètodes. Aquests seran:
    - validaCampNoBuit(String camp)
    - validaTamanyMaximCamp(String camp, int tamanyMaxCamp)
    - validaCampEmail(String camp)
    - validaIntervalNumero(int intMinim, int intMaxim)
*/
public class ValidadorCampos
{

  public ValidadorCampos() {
      
  }
  
  public boolean validaCampNoBuit(String camp) {
     return camp!=null && !camp.isEmpty(); // Otras opciones, camp.equals("");
  }
  
  public boolean validaPrimeraLletraMajuscules(String camp) {
//     String primeraLletra = camp.substring(0,1);
//     String primeraLletraMajuscula = camp.substring(0,1).toUpperCase();
//     return primeraLletra.equals(primeraLletraMajuscula); // Otras opciones, camp.equals("");
        String regex = "^[A-Z][a-z]+$";
       return camp.matches(regex);
  }
  
  public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
     return camp.length()>tamanyMaxCamp;
  }
    
  public boolean validaCampEmail(String email) {
     // https://www.tutorialspoint.com/validate-email-address-in-java 
     String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
     return email.matches(regex);
  }
  
    
  public boolean validaCampURL(String email) {
     // https://www.tutorialspoint.com/validate-email-address-in-java 
     String regex = "(http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?";
     return email.matches(regex);
  }
  
  public boolean validaIntervalNumero(int camp, int numMin, int numMax) {
     return true;
  }
  
}

