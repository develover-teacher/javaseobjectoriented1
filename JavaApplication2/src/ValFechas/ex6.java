package ValFechas;

/**
 * 24. Crear un joc de proves per validar dates amb les següents regles: 
    a) L'any ha d'estar comprès entre 1900 i 2100 (tots dos inclosos) 
    b) Ha de complir el format: dd/mm/aaaa: ajuda: if(Pattern.matches("\\d{1,2}/\\d{1,2}/\\d{1,4}", data)) 
    c) Ha de validar els dies segons el mes. Compte amb els anys de traspàs
    d) Finalment imprimirà el dia següent de la data validada
    Es crearan dues classes: la principal i la classe que fa la validació.
 * @author DAWBIO-2
 */
public class ex6 {
   
    private static validateDate nd;
    public static void main(String[] args) {
       
       String message="";
       //Es crea un objecte de la classe validarData
        nd = new validateDate();
        if (nd.validarFecha("02/04/2017")){
                
             message="";
        }else{
             message="La teva data 26/09/2017 no és valida";
        }
        printing(message);
       // nd = new validateDate();
       if (nd.validarFecha("01/aa/2011")){
         message="";
        
       }else{
         message="La teva data 01/aa/2011 no és valida";

       }
        printing(message);
//        nd = new validateDate();
        if(nd.validarFecha("01/10/11")){
             message="";
        }else{
         message="La teva data 01/10/11 no és valida";

       }
         printing(message);
//        nd = new validateDate();
        if(nd.validarFecha("10/5/1789")){
           message="";
        }else{
         message="La teva data 10/5/1789 no és valida";

       }
         printing(message);
    }

   /**
    * This function displays a message
    * @param data is a message or the next date
    * 
    */
  
    private static void printing(String data) {
    
       if(data.equals("")){
            System.out.println("next day: " + nd.nextDay());
       }else{
            System.out.println(data);  
       }
       System.out.println();
    
    }
    
    
}
