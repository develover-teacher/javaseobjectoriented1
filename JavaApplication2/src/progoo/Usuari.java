/*
 * How to autogenerate constructor, getters and setters in Apache NB.
 * Source -> Insert Code -> ... 
 * https://coderwall.com/p/oyanzg/auto-generate-get-set-constructor-functions-in-netbeans
 */
package progoo;

public class Usuari {
    private String nom;
    private String password;
    private String rol;
    private int salari;
    private boolean logged;

    public Usuari(String nom, String rol, int salari) {
        this.nom = nom;
        this.rol = rol;
        this.salari = salari;
    }

    public boolean login(String nom, String password) {
        return this.nom.equals(nom) && this.password.equals(password);
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getSalari() {
        return salari;
    }

    public void setSalari(int salari) {
        this.salari = salari;
    }
    
    
    
    
}
