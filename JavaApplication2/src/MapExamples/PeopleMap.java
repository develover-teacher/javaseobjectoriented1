package MapExamples;

import java.util.HashMap;
import java.util.Map;

/**
 *ExCol3. Crea un HashMap de Persones, amb el nom com a clau i la persona com a contingut.
És a dir, un HashMap.
Insereix 4 elements. Prova: 
- La cerca per clau
- La inserció
- Esborrar
- Mostrar tots els elements
 */
public class PeopleMap {
    
    private static Map<String,PersonaBean> people = new HashMap<String,PersonaBean> ();
    
    private static void crearPersones() {
        people.put("Laura",new PersonaBean("Laura",21,"Precariado") );
        people.put("Andrea",new PersonaBean("Andrea",23,"Profesora") );
        people.put("Dani",new PersonaBean("Dani",19,"Estudiante") );
        people.put("Pau",new PersonaBean("Pau",25,"Precariado") );
    }
    
    // https://stackoverflow.com/questions/5920135/printing-hashmap-in-java
    private static void mostrarPersones() {
        for (String nombrePersona: people.keySet()){
            String clave = nombrePersona;
            PersonaBean persona = people.get(clave);  
            System.out.println(clave + " " + persona);  
        } 
    }
    
    
    public static void main(String[] args) {
        crearPersones();
        mostrarPersones();
    }
}
