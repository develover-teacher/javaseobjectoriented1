package personas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** 
 * Ejercicio para probar las colecciones, el ArrayList.
 * @author mique
 */
public class ListaPersonas {
    
    private static void mostrarPersones() {
        for (PersonaBean nombrePersona : personas) {
            System.out.println(nombrePersona);
        }
    }
    
    private static void crearPersones() {
        personas.add(new PersonaBean("Laura",21,"Precariado") );
        personas.add(new PersonaBean("Andrea",23,"Profesora") );
        personas.add(new PersonaBean("Dani",19,"Estudiante") );
        personas.add(new PersonaBean("Pau",25,"Precariado") );
    }

    private static List<PersonaBean> personas = new ArrayList<>();
        // JAVA 6 y anteriores.
        // List<Persona> personas = new ArrayList<Persona>();
    
    public static void main(String[] args) {
        
        crearPersones();
        mostrarPersones();
        personas.add(new PersonaBean("Ester",27,"Diseñadora"));
        personas.add(new PersonaBean("Juan",18,"Estudiante"));
        // Collections.sort(personas);
        mostrarPersones();
        System.out.println("");
    }
}
