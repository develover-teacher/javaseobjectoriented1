package personas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** 
 * ExCol1. Crea una llista de noms de persona (de tipus String). Prova el següent: 
- mostrar tots els noms,
- buscar-ne un per id.
- comprova si un nom existeix o no a la llista
- esborrar algun nom
- mostrar la llista ordenada alfabèticament.
 */
public class EjCol1_ListaNombres {
    
    private static void mostrarLista(List<String> nombresPersonas) {
        for (String nombrePersona : nombresPersonas) {
            System.out.println(nombrePersona);
        }
    }
    
    public static void main(String[] args) {
        
        List<String> nombresPersonas = new ArrayList<>();
        // JAVA 6 y anteriores.
        // List<String> nombresPersonas = new ArrayList<String>();
        nombresPersonas.add("Berto");
        nombresPersonas.add("David");
        nombresPersonas.add("Eli");
        nombresPersonas.add("Andrea");
        nombresPersonas.add("Sara");
        nombresPersonas.add("Carlos");
        nombresPersonas.add("Pau");
        
        System.out.println("Demo ArrayList<String>");
        mostrarLista(nombresPersonas);
        
        System.out.println("Metodes ArrayList<String>");
        // Get By ID (Per 
        System.out.println("Posicio 1 " + nombresPersonas.get(1));
        // Get by name
        System.out.println("Eli => " 
                + nombresPersonas.get(nombresPersonas.indexOf("Eli")));
        // Contains
        System.out.println("Esta David ?" + nombresPersonas.contains("David"));
        System.out.println("Esta Puala ?" + nombresPersonas.contains("Paula"));
        
        // Remove
        System.out.println("David abandonando grupo ");
        nombresPersonas.remove("David");
        System.out.println("Esta David ?" + nombresPersonas.contains("David"));

        // Ordenar
        Collections.sort(nombresPersonas);
        System.out.println("Lista ordenada.");
        mostrarLista(nombresPersonas);
        
        // Ordenar inversa.
        // Collections.sort(nombresPersonas, );
        
    }
}
