package MapExample_Optional;

import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;
/**
 *
 * Java program to demonstrate What is NavigableMap in Java and How to use NavigableMap
 * in Java. NavigableMap provides two important features navigation methods
 * like lowerKey(), floorKey, ceilingKey() and higherKey().
 * There Entry counterpart and methods to create subMap e.g. headMap(), tailMap()
 * and subMap().
 *
 * @author Javin Paul
 */
public class MapaNavegableTrens {

    public static void main(String args[]) {
     
        // NavigableMap extends SortedMap to provide useful navigation methods,
        // and TreeSet to autosort.
        NavigableSet<String> navigableMap = new TreeSet<String>();
     
        navigableMap.add("1224");
        navigableMap.add("1150");
        navigableMap.add("1321");
        navigableMap.add("1421");
        navigableMap.add("0956");
        navigableMap.add("1034");
     
        for (String string : navigableMap) {
            System.out.println(string);
        }
        
        //lower returns key which is less than specified key
        System.out.println("lowerKey from Java : " + navigableMap.lower("1150"));
     
        //floor returns key which is less than or equal to specified key
        System.out.println("floorKey from Java: " + navigableMap.floor("1150"));
     
        //ceiling returns key which is greater than or equal to specified key
        System.out.println("ceilingKey from Java: " + navigableMap.ceiling("1224"));
     
        //higher returns key which is greater specified key
        System.out.println("higherKey from Java: " + navigableMap.higher("1224"));
     
     
        //Apart from navigagtion methodk, it also provides useful method
        //to create subMap from existing Map e.g. tailMap, headMap and subMap

        SortedSet<String> subSet = navigableMap.subSet("0941","1251");
        System.out.println("subSet created form navigableMap : " + subSet);
     
        //an example of subMap - return NavigableMap from toKey to fromKey
//        NavigableMap<String, String> subMap = navigableMap.subMap("C++", false ,
//                                                                  "Python", false);
    }
}