/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex4demo;

import java.util.ArrayList;
import java.util.List;
import personas.PersonaBean;

/**
 *
 * @author tarda
 */
public class LlistaComentaris {
    
     private static void mostrarComentaris() {
        for (Comentaris nombrePersona : comentaris) {
            System.out.println(nombrePersona);
        }
    }
    
    private static void crearComentaris() {
        comentaris.add(
                new Comentaris("mamorosal", "m.a@g.com", "ma.gitlab.com", "Hola!"));
        comentaris.add(
                new Comentaris("user1", "m.a@g.com", "user1.gitlab.com", "Hola!"));
    }
    
    private static void  trobarComentariNomUsuari(String nom) {
        Comentaris resultat = new Comentaris();
        boolean trobat = false;
        for (int i = 0; i < comentaris.size()-1 && !trobat; i++) {
            resultat = comentaris.get(i);
            if(resultat.getNom().equals(nom)) {
                trobat = true;
            }
        }
        if(trobat) {
            System.out.println(resultat);
        } else {
            System.out.println("No trobat");
        }
        // return resultat;
    }
    
    static List<Comentaris> comentaris 
                = new ArrayList<>();
    
    public static void main(String[] args) {
        crearComentaris();
        mostrarComentaris();
        System.out.println("Primer comentari mamorosal ??");
        trobarComentariNomUsuari("mamorosal");
        trobarComentariNomUsuari("admin");
        
//        Comentaris comentari1 = new Comentaris(nom, email, web, comentari);
//        Comentaris comentari2 = new Comentaris(nom, email, web, comentari);
//        Comentaris comentari3 = new Comentaris(nom, email, web, comentari);
//        Comentaris comentari4 = new Comentaris(nom, email, web, comentari);
//        comentaris.add();
//        comentaris.add();
//        comentaris.add();
//        comentaris.add();
    }
}
