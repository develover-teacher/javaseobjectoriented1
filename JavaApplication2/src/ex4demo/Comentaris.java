/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex4demo;

/**
 *
 * @author tarda
 */
public class Comentaris {
    String nom;
    String email;
    String web;
    String comentari;

    /**
     * 
     * @param nom
     * @param email
     * @param web
     * @param comentari 
     */
    public Comentaris(String nom, String email, String web, String comentari) {
        this.nom = nom;
        this.email = email;
        this.web = web;
        this.comentari = comentari;
    }
     public Comentaris() {

    }


    @Override
    public String toString() {
        return "Comentaris{" + "nom=" + nom + ", email=" + email + ", web=" + web + ", comentari=" + comentari + '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getComentari() {
        return comentari;
    }

    public void setComentari(String comentari) {
        this.comentari = comentari;
    }
    
    
}
