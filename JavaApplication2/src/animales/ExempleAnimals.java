package animales;

import progoo.*;
import java.util.Arrays;

/**
 * Clase que mostra per pantalla diversos Animals creats amb la clase 
 * Animal.
 * @version 1.0
 * @author mati
 */
public class ExempleAnimals {
    
    public static void main(String[] args) {
        
        String salutacio = "Prova POO Animals.";
        System.out.println(salutacio);
        
        Animal a1 = new Animal("Gat",4,2);
        Animal a2 = new Animal("Aranya",8,2);
        Animal a3 = new Animal("Serp",0,2);
        Animal a4 = new Animal("Cienpies",100,2);
        Animal a5 = new Animal(4,2);
        // Canvi parametres.
        System.out.println(a1);
        // PETA!
        // a1.patas=5;
        a1.nombre="Gos";
        a1.setPatas(5);
        System.out.println(a1);
        
        
        System.out.println(salutacio);
        Animal[] granja = {a1,a2,a3,a4,a5};
        for (Animal animal : granja) {
            System.out.println(animal.habla());
        }
        
    }
}
