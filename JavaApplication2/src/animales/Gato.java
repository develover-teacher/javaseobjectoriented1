package animales;
/**
 * Clase que representa un animal por su nombre, numero de patas y ojos.
 * @author Miquel
 */
public class Gato extends Animal
{
  // Visibilidad de los atributos.
  public String nombre;
  protected int patas;
  private int ojos;
  
  // Sobrecarga de constructores.
  public Gato(String nombre, int patas, int ojos) {
    super(nombre,patas,ojos);
  }
  
  public String habla() { 
     return "Soy un " + nombre + " y hago miau!!"; 
  }
  
  public String ronronea() { 
     return "RRRRRR"; 
  }
}

