/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

import java.util.Objects;

public class Pokemon {
    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pokemon other = (Pokemon) obj;
        if (this.nivel != other.nivel) {
            return false;
        }
        if (this.energiaAct != other.energiaAct) {
            return false;
        }
        if (this.energiaMax != other.energiaMax) {
            return false;
        }
        if (Double.doubleToLongBits(this.ataque) != Double.doubleToLongBits(other.ataque)) {
            return false;
        }
        if (Double.doubleToLongBits(this.defensa) != Double.doubleToLongBits(other.defensa)) {
            return false;
        }
        if (Double.doubleToLongBits(this.velocidad) != Double.doubleToLongBits(other.velocidad)) {
            return false;
        }
        if (Double.doubleToLongBits(this.defensaEsp) != Double.doubleToLongBits(other.defensaEsp)) {
            return false;
        }
        if (Double.doubleToLongBits(this.ataqueEsp) != Double.doubleToLongBits(other.ataqueEsp)) {
            return false;
        }
        if (!Objects.equals(this.Pok_id, other.Pok_id)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.tipo1, other.tipo1)) {
            return false;
        }
        if (!Objects.equals(this.tipo2, other.tipo2)) {
            return false;
        }
        return true;
    }
    
}