package animales;
/**
 * Clase que representa un animal por su nombre, numero de patas y ojos.
 * @author Miquel
 */
public class Animal 
{
  // Visibilidad de los atributos.
  public String nombre;
  protected int patas;
  private int ojos;
  
  // Sobrecarga de constructores.
  public Animal(String nombre, int patas, int ojos) {
    this.nombre = nombre;
    this.patas = patas;
    this.ojos = ojos;
  }
  
  public Animal(int patas, int ojos) {
    nombre = "animal";
    this.patas = patas;
    this.ojos = ojos;
  }
  
 /**
 * <strong>Mètode que puede modificar el numero de patas de un Animal.</strong>
 * @version 1.0
 * @param patas Numero de de potes. 
 */
  public void setPatas(int patas) {
    this.patas = patas;
  }
  
  public int getPatas() {
    return patas;
  }
  
  public void setOjos(int ojos) {
    this.ojos = ojos;
  }
  
  public int getOjos() {
    return ojos;
  }
  
  /**
 * Mètode que retorna tota la info d'un Animal.
 * @version 1.0
 * @author mati
 * @return String amb tota la info de l'Animal
 */
  public String habla() { 
     return "Soy un " + nombre + " " + " de " + patas + " patas y " + ojos + " ojos"; 
  } 
}

