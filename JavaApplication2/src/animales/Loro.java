package animales;
/**
 * Clase que representa un animal por su nombre, numero de patas y ojos.
 * @author Miquel
 */
public class Loro extends Animal
{

  // Sobrecarga de constructores.
  public Loro(String nombre, int patas, int ojos) {
    super(nombre,patas,ojos);
  }
  
  public String habla() { 
     return "Soy un " + nombre + " hablo y canto."; 
  }
  
  public String cantar() { 
     return "Breec"; 
  }
  
}

